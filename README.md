# creativity-test

## Objectifs 

Le but premier de l'exercice est de réaliser une application simple.

Page Web (FrontEnd) --> API REST (Backend)

Aucun langage imposé, aucune technologie imposée, il faut juste que je puisse récupérer le code et le lancer sur ma machine.

Conseils : penser à ajouter les instructions, les prérequis, la documentation pour lancer le bout de code, genre un README qui explique comment mettre en place la base de données (du coup, évitons de choisir DB2 ou Oracle comme DB, un SQLite est bien plus simple, ou alors un H2, si on est sur du Spring/Java)

Les réponses non acceptées :

* "ça marche pourtant sur ma machine !",
* "ah oueh, mais j'ai utilisé le JDK IBM, forcément, ça buggue",
* "j'ai tout fait en COBOL, tu ne peux pas comprendre",

Le code peut-être transmis: 

* par mail ZIP,
* par Pull request,
* par la RFC 1149 si vous avez du courage  : 
	- https://tools.ietf.org/html/rfc1149 
	- https://fr.wikipedia.org/wiki/IP_over_Avian_Carriers


## Santé !!

Le but est de designer une API REST afin de fournir des informations simples.

La première route à implémenter est de type:
```
GET /health
```

dont la réponse attendue est un JSON du type: 
```
{
	"status" : "isalive"
}
```

Cet API peut être contrôlée par le frontend par l'intermédiaire d'un simple button qui affiche une popup avec le contenu, ou alors une image, une pastille verte, peut importe du moment qu'on valide le retour.


## Manipulation quelconque de données

La deuxième route à implémenter récupère des informations pour les compiler dans un autre format à la sortie

```
POST /compute
{ 
	"firstname": String
	"lastname": String
	"birthdate": YYYY-MM-dd
}
```

Réponse attendue : 
```
{
	"score": Integer (règle = Nb de char dans firstname + Nb de char dans lastname + YYYYMMdd)
}
```

Le frontend associé proposera de saisir ces trois champs et d'afficher le résultat.


## Dernière opération - la persistance

L'idée est que chaque POST permet d'enregistrer les trois champs et la réponse dans une base de données en plus de répondre.

Cette base de données doit contenir deux tables :

* celle contenant les trois champs demandés (firstname, lastname, birthdate)
* celle contenant la réponse (score)

La dernière étape serait de réaliser une route du type
```
GET /requests?query=Sébastien
```

qui retourne l'ensemble des entrées de la base sous forme d'un JSON où la recherche s'est faite sur l'ensemble des 4 champs sous une forme attendue de ce type :
```
[
	{
		"id" : 1,
		"fistname" : "Sébastien",
		"lastname" : "BOULARD",
		"birthdate" : "2004-04-03",
		"score" : "..."
	},
	{
		"id" : 5,
		"fistname" : "Sébastien",
		"lastname" : "MICHEL",
		"birthdate" : "1980-04-02",
		"score" : "..."
	}
]
```

Le frontend pourrait permettre d'afficher la liste des résultats.


## Subsidiaire

Si en plus, vous avez ajouté des tests ... (unitaires, fonctionnels, intégration ...).


## Quelques conseils

- Faites le maximum que vous pouvez : ce n'est pas nécessaire de tout faire car les derniers points peuvent être un peu longs à réaliser)
- L'idée est plus de se timeboxer sur 1-2h maximum et de voir ce qui a été possible de faire et de voir ce qui manquait pour finir l'exercice.
- Quelques mots clefs:  CORS, API REST, Postman, git, H2, crêpes ...

